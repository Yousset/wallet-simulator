# Wallet Simulator

Yousset Wallet Simulator

## Install

This project uses Java 11 to run. 

Based in Spring Boot and features.

### Installing Java 11 on Ubuntu

1. Install OpenJdk 11, and update JAVA_HOME  
    ```bash
    sudo apt install openjdk-11-jdk
    update-alternatives --config java
    # choose java 11 and copy the path to java 11 bin
    export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
    ```
    
2. Build the project

    ```bash
    mvn clean install
    ```
    
## Running

To compile and run the app, type this in a terminal. 

    mvn spring-boot:run 

You can ping the `health-check` endpoint to verify that the app is up and running.

    curl http://localhost:9290/wallet/health-check


## Swagger

API documentation is accessible from [localhost:9290/wallet/swagger-ui.html](http://localhost:9290/wallet/swagger-ui.html)

Endpoints can be documented via annotations. Follow instructions detailed in this blog post: [springfox](https://springfox.github.io/springfox/docs/current/)

## Services

###GET
- [/wallet/api/coins-prices](http://localhost:9290/wallet/api/coins-prices) The Service provide all available coins, with your prices. this service is pageable and is cached
- [/wallet/api/wallet](http://localhost:9290/wallet/api/wallet) The Service provide all active wallets, this service is pageable
- [/wallet/api/wallet-all](http://localhost:9290/wallet/api/wallet-all) The Service provide all wallets inclusive the deleted, this service is pageable
- [/wallet/api/wallet/{id}](http://localhost:9290/wallet/api/wallet/{id}) The Service provide, a full detail wallet with your accounts

###POST
- [/wallet/api/wallet](http://localhost:9290/wallet/api/wallet) The service allows create a wallet Example:
```
{
  "initialBalance": 1.0, // Required, Min value 0
  "mainCoin": "string", // Required
  "name": "string" // Required
}
```

- [/wallet/api/wallet/{id}/debit](http://localhost:9290/wallet/api/wallet/{id}/debit) The service allows you to withdraw currencies from your balance Example:
```
{
  "balance": 1.0, // Required
  "coin": "string" //Optional, If the value is null, the default is the primary coin of your wallet
}
```

- [/wallet/api/wallet/{id}/credit](http://localhost:9290/wallet/api/wallet/{id}/credit) The service allows add coin in your balance Example:
```
{
  "balance": 1.0, // Required
  "coin": "string" //Optional, If the value is null, the default is the primary coin of your wallet
}
```

- [/wallet/api/wallet/{id}/buy](http://localhost:9290/wallet/api/wallet/{id}/buy) The service allows buy coin Example:
```
{
  "amount": 1.0, // Required
  "coinBase": "string", //Optional, If the value is null, the default is the primary coin of your wallet
  "coinToBuy": "string" // Required
}
```

- [/wallet/api/wallet/{id}/transfer](http://localhost:9290/wallet/api/wallet/{id}/transfer) The service allows you to transfer coins between wallet accounts Example:
```
{
  "amount": 1.0, // Required
  "coinBase": "string", //Optional, If the value is null, the default is the primary coin of your wallet
  "coinToTransfer": "string", //Optional, If the value is null, the default is the primary coin of your wallet
  "wallet": 1 // Required
}
```

###PUT
- [/wallet/api/wallet/{id}](http://localhost:9290/wallet/api/wallet/{id}) The service allows edit a wallet Example:
```
{
  "name": "string" // Required
}
```

###DELETE
- [/wallet/api/wallet/{id}](http://localhost:9290/wallet/api/wallet/{id}) The service allows delete a wallet 
