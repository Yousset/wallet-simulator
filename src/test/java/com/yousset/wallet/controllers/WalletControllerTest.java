package com.yousset.wallet.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yousset.wallet.requests.CreateWallet;
import com.yousset.wallet.services.WalletService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.server.ResponseStatusException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(
        controllers = WalletController.class,
        properties = {"myconfig.initialize.coins=false"}
)
public class WalletControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletService walletService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void whenCreateAndValidInputFailThenReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/wallet")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(CreateWallet.builder().initialBalance(-1.0).build())))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenCreateAndSuccessThenReturnsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/wallet")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(CreateWallet.builder()
                        .name("Wallet")
                        .mainCoin("BTC")
                        .initialBalance(1.0).build())))
                .andExpect(status().isOk());
    }

    @Test
    void whenCreateAndFailCoinThenReturnsNotFound() throws Exception {
        when(walletService.createWallet(any(CreateWallet.class))).thenCallRealMethod();
        when(walletService.findCoin(anyString())).thenThrow(new ResponseStatusException(NOT_FOUND, "Invalid Coin Symbol Code"));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/wallet")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(CreateWallet.builder()
                        .name("Wallet")
                        .mainCoin("BTC")
                        .initialBalance(1.0).build())))
                .andExpect(status().isNotFound());
    }

    //TODO Se que Faltan test para el coverage, pero con los que hice cubre las funcionalidades basicas y configuraciones

}
