package com.yousset.wallet.services;

import com.yousset.wallet.daos.CoinsDao;
import com.yousset.wallet.daos.WalletAccountsDao;
import com.yousset.wallet.daos.WalletDao;
import com.yousset.wallet.models.Coins;
import com.yousset.wallet.models.Wallet;
import com.yousset.wallet.models.WalletAccounts;
import com.yousset.wallet.requests.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Pageable;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import static com.yousset.wallet.services.WalletService.KEY_PRICE;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("ALL")
@SpringBootTest(
        properties = {"myconfig.initialize.coins=false"}
)
public class WalletServiceTest {

    @Autowired
    WalletService service;
    @MockBean
    CryptoCompareService cryptoCompareService;
    @MockBean
    CoinsDao coinsDao;
    @MockBean
    WalletDao walletDao;
    @MockBean
    WalletAccountsDao walletAccountsDao;
    @Autowired
    CacheManager cacheManager;

    @BeforeEach
    public void setup() {
        cacheManager.getCacheNames().forEach(name -> {
            cacheManager.getCache(name).clear();
        });
        reset(coinsDao);
    }

    @Test
    public void whenGetAllCurrencyPriceIsDataCacheThenShouldReturnListFromCache() throws IOException {
        cacheManager.getCache("currencyPrices").put(KEY_PRICE, Arrays.asList(Coins.builder().symbol("BTC").build()));
        Iterable<Coins> coins = service.getAllCurrencyPrice();
        assertNotNull(coins);
        assertEquals(coins.iterator().next().getSymbol(), "BTC");
        verify(coinsDao, never()).findAll();
    }

    @Test
    public void whenGetAllCurrencyPriceDataNoCacheThenShouldReturnListFromDB() throws IOException {
        when(coinsDao.findAll()).thenReturn(Arrays.asList(Coins.builder().symbol("BTC").build()));
        Iterable<Coins> coins = service.getAllCurrencyPrice();
        assertNotNull(coins);
        assertEquals(coins.iterator().next().getSymbol(), "BTC");
        verify(coinsDao, times(1)).findAll();
    }

    @Test
    public void whenGetSingleCurrencyPriceIsDataCacheThenShouldReturnListFromCache() throws IOException {
        cacheManager.getCache("singleCurrencyPrice").put(KEY_PRICE, singletonMap("USD", 1.0));
        Map<String, Double> prices = service.getSingleCurrencyPrice(KEY_PRICE, Arrays.asList("USD"));
        assertNotNull(prices);
        assertEquals(prices.get("USD"), 1.0);
        verify(cryptoCompareService, never()).getPrices(anyString(), anyList());
    }

    @Test
    public void whenGetSingleCurrencyPriceDataNoCacheThenShouldReturnListFromDB() throws IOException {
        when(cryptoCompareService.getPrices(anyString(), anyList())).thenReturn(singletonMap("USD", 1.0));
        Map<String, Double> prices = service.getSingleCurrencyPrice(KEY_PRICE, Arrays.asList("USD"));
        assertNotNull(prices);
        assertEquals(prices.get("USD"), 1.0);
        verify(cryptoCompareService, times(1)).getPrices(anyString(), anyList());
    }

    @Test
    public void whenFindCoinSuccessThenShouldReturnCoin() throws IOException {
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build()));
        Coins coins = service.findCoin("BTC");
        assertNotNull(coins);
        assertEquals(coins.getSymbol(), "BTC");
        verify(coinsDao, times(1)).findOneBySymbol(anyString());
    }

    @Test
    public void whenFindCoinFailThenShouldThrowException() throws IOException {
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.empty());
        assertThrows(
                ResponseStatusException.class, () -> service.findCoin("BTC"));
        verify(coinsDao, times(1)).findOneBySymbol(anyString());
    }

    @Test
    public void whenFindWalletSuccessThenShouldReturnCoin() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").build()));
        Wallet wallet = service.findWallet(1L);
        assertNotNull(wallet);
        assertEquals(wallet.getName(), "Wallet");
        verify(walletDao, times(1)).findById(anyLong());
    }

    @Test
    public void whenFindWalletFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(
                ResponseStatusException.class, () -> service.findWallet(1L));
        verify(walletDao, times(1)).findById(anyLong());
    }

    @Test
    public void whenCreateWalletSuccesThenShouldReturnNewWallet() throws IOException {
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build()));
        when(walletDao.save(any(Wallet.class))).thenReturn(Wallet.builder().name("Wallet").id(1L).build());
        when(walletAccountsDao.save(any(WalletAccounts.class))).thenReturn(WalletAccounts.builder().id(1L).build());

        Wallet wallet = service.createWallet(CreateWallet.builder()
                .initialBalance(10.0)
                .mainCoin("BTC")
                .name("YOUSSET")
                .build());

        assertNotNull(wallet);
        assertNotNull(wallet.getId());
    }

    @Test
    public void whenCreateWalletFailCoinThenShouldThrowException() throws IOException {
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.empty());
        assertThrows(
                ResponseStatusException.class, () -> service.createWallet(CreateWallet.builder()
                        .initialBalance(10.0)
                        .mainCoin("BTC")
                        .name("YOUSSET")
                        .build()));
        verify(walletDao, never()).save(any(Wallet.class));
    }

    @Test
    public void whenUpdateWalletSuccesThenShouldReturnWallet() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").build()));
        when(walletDao.save(any(Wallet.class))).thenReturn(Wallet.builder().name("New Name").id(1L).build());

        Wallet wallet = service.updateWallet(1L, UpdateWallet.builder()
                .name("YOUSSET")
                .build());

        assertNotNull(wallet);
        assertEquals(wallet.getName(), "New Name");
    }

    @Test
    public void whenUpdateWalletFailWalletThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(
                ResponseStatusException.class, () -> service.updateWallet(1L, UpdateWallet.builder()
                        .name("YOUSSET")
                        .build()));
        verify(walletDao, never()).save(any(Wallet.class));
    }

    @Test
    public void whenDeleteWalletSuccesThenShouldReturnWallet() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").build()));
        when(walletDao.save(any(Wallet.class))).thenReturn(Wallet.builder().name("New Name").status(0).id(1L).build());

        Wallet wallet = service.deleteWallet(1L);

        assertNotNull(wallet);
        assertEquals(wallet.getName(), "New Name");
        assertEquals(wallet.getStatus(), 0);
    }

    @Test
    public void whenDeleteWalletFailWalletThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(
                ResponseStatusException.class, () -> service.deleteWallet(1L));
        verify(walletDao, never()).save(any(Wallet.class));
    }

    @Test
    public void whenGetWalletsTestingThenShouldBeSucess() throws IOException {
        when(walletDao.findAllByStatusEquals(anyInt(), any(Pageable.class))).thenReturn(singletonList(Wallet.builder().name("Wallet").build()));
        when(walletDao.findAll()).thenReturn(singletonList(Wallet.builder().name("Wallet").build()));

        service.getWallets(true, mock(Pageable.class));
        verify(walletDao, times(1)).findAllByStatusEquals(anyInt(), any(Pageable.class));
        verify(walletDao, never()).findAll();

        reset(walletDao);

        service.getWallets(false, mock(Pageable.class));
        verify(walletDao, never()).findAllByStatusEquals(anyInt(), any(Pageable.class));
        verify(walletDao, times(1)).findAll(any(Pageable.class));
    }

    @Test
    public void whenDebitCreditWalletAndWalletFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.empty());

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.debitCreditWallet(1L,
                        DebitCreditWallet.builder()
                                .balance(10.0)
                                .coin("BTC")
                                .build(), true));
        assertTrue(e.getMessage().contains("Wallet not found"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());

        reset(walletDao);
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(0).build()));

        e = assertThrows(
                ResponseStatusException.class, () -> service.debitCreditWallet(1L,
                        DebitCreditWallet.builder()
                                .balance(10.0)
                                .coin("BTC")
                                .build(), true));
        assertTrue(e.getMessage().contains("your wallet is deleted"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenDebitCreditWalletAndCoinFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.empty());

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.debitCreditWallet(1L,
                        DebitCreditWallet.builder()
                                .balance(10.0)
                                .coin("BTC")
                                .build(), true));
        assertTrue(e.getMessage().contains("Invalid Coin Symbol Code"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenDebitCreditWalletAndFailNegativeThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString())).thenReturn(Optional.empty());

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.debitCreditWallet(1L,
                        DebitCreditWallet.builder()
                                .balance(10.0)
                                .coin("BTC")
                                .build(), false));
        assertTrue(e.getMessage().contains("your account cannot be negative"));
        verify(walletAccountsDao, never()).save(any(WalletAccounts.class));
    }

    @Test
    public void whenDebitCreditWalletIsSuccesThenShouldReturnNewBalance() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString())).thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).build()));
        when(walletAccountsDao.save(any(WalletAccounts.class))).then((invocationOnMock) -> invocationOnMock.getArguments()[0]);


        WalletAccounts accounts = service.debitCreditWallet(1L,
                DebitCreditWallet.builder()
                        .balance(10.0)
                        .coin("BTC")
                        .build(), false);

        assertEquals(accounts.getBalance(), BigDecimal.valueOf(90.0));

        accounts = service.debitCreditWallet(1L,
                DebitCreditWallet.builder()
                        .balance(10.0)
                        .coin("BTC")
                        .build(), true);

        assertEquals(accounts.getBalance(), BigDecimal.valueOf(100.0));

        verify(walletAccountsDao, times(2)).save(any(WalletAccounts.class));
    }

    @Test
    public void whenBuyCoinAndWalletFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.empty());

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.buyCoin(1L,
                        BuyCoin.builder()
                                .coinToBuy("BTC")
                                .coinBase("BTC")
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Wallet not found"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());

        reset(walletDao);
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(0).build()));

        e = assertThrows(
                ResponseStatusException.class, () -> service.buyCoin(1L,
                        BuyCoin.builder()
                                .coinToBuy("BTC")
                                .coinBase("BTC")
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("your wallet is deleted"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenBuyCoinAndCoinFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.empty());
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString())).thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).build()));

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.buyCoin(1L,
                        BuyCoin.builder()
                                .coinToBuy("BTC")
                                .coinBase("BTC")
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Invalid Coin Symbol Code"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());

        reset(coinsDao);
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build())).thenReturn(Optional.empty());

        e = assertThrows(
                ResponseStatusException.class, () -> service.buyCoin(1L,
                        BuyCoin.builder()
                                .coinToBuy("BTC")
                                .coinBase("BTC")
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Invalid Coin Symbol Code"));
        verify(walletAccountsDao, times(1)).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenBuyCoinAndAccountFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString())).thenReturn(Optional.empty());

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.buyCoin(1L,
                        BuyCoin.builder()
                                .coinToBuy("BTC")
                                .coinBase("BTC")
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Wallet Account: "));
        verify(walletAccountsDao, times(1)).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenBuyCoinAndPriceFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").mainCoin("BTC").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BCH").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString()))
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").build()))
                .thenReturn(Optional.empty())
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").build()))
                .thenReturn(Optional.empty());
        when(cryptoCompareService.getPrices(anyString(), anyList()))
                .thenReturn(singletonMap("ANY", 1.0))
                .thenReturn(singletonMap("BTC", 0.089));

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.buyCoin(1L,
                        BuyCoin.builder()
                                .coinToBuy("BCH")
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Invalid Coin, price not found"));

        e = assertThrows(
                ResponseStatusException.class, () -> service.buyCoin(1L,
                        BuyCoin.builder()
                                .coinToBuy("BCH")
                                .amount(BigDecimal.valueOf(1000000.0))
                                .build()));
        assertTrue(e.getMessage().contains("Insufficient balance to buy coin"));
        verify(walletAccountsDao, never()).save(any(WalletAccounts.class));
    }

    @Test
    public void whenBuyCoinSucessThenShouldShouldBeReturnNewBalance() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").mainCoin("BTC").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BCH").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString()))
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").id(1L).build()))
                .thenReturn(Optional.empty());
        when(cryptoCompareService.getPrices(anyString(), anyList())).thenReturn(singletonMap("BTC", 0.089));
        when(walletAccountsDao.save(any(WalletAccounts.class)))
                .then((invocationOnMock) -> {
                    WalletAccounts accounts = (WalletAccounts) invocationOnMock.getArguments()[0];
                    BigDecimal tmp = BigDecimal.valueOf(2.0).multiply(BigDecimal.valueOf(0.089));
                    tmp = BigDecimal.valueOf(100.0).subtract(tmp);
                    assertEquals(accounts.getBalance(), tmp);
                    return accounts;
                })
                .then((invocationOnMock) -> invocationOnMock.getArguments()[0]);

        WalletAccounts accounts = service.buyCoin(1L,
                BuyCoin.builder()
                        .coinToBuy("BCH")
                        .amount(BigDecimal.valueOf(2.0))
                        .build());

        assertEquals(accounts.getBalance(), BigDecimal.valueOf(2.0));
        assertNotNull(accounts.getCurrentPrice());
    }

    @Test
    public void whenTransferAndWalletFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong()))
                .thenReturn(Optional.empty())
                .thenReturn(Optional.of(Wallet.builder().name("Wallet").status(0).build()))
                .thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()))
                .thenReturn(Optional.empty())
                .thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()))
                .thenReturn(Optional.of(Wallet.builder().name("Wallet").status(0).build()));

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Wallet not found"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());

        e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("your wallet is deleted"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());

        e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Wallet not found"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());

        e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Wallet to transfer is deleted"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenTransferAndCoinFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.empty());
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString())).thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).build()));

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Invalid Coin Symbol Code"));
        verify(walletAccountsDao, never()).findOneByWalletAndCoin(any(Wallet.class), anyString());

        reset(coinsDao);
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build())).thenReturn(Optional.empty());

        e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Invalid Coin Symbol Code"));
        verify(walletAccountsDao, times(1)).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenTransferAndAccountFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BTC").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString())).thenReturn(Optional.empty());

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Wallet Account: "));
        verify(walletAccountsDao, times(1)).findOneByWalletAndCoin(any(Wallet.class), anyString());
    }

    @Test
    public void whenTransferAndPriceFailThenShouldThrowException() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").mainCoin("BTC").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BCH").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString()))
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").build()))
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").build()))
                .thenReturn(Optional.empty())
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").build()))
                .thenReturn(Optional.empty());
        when(cryptoCompareService.getPrices(anyString(), anyList()))
                .thenReturn(singletonMap("ANY", 1.0))
                .thenReturn(singletonMap("BTC", 0.089));

        when(walletAccountsDao.save(any(WalletAccounts.class))).then((invocationOnMock) -> invocationOnMock.getArguments()[0]);

        ResponseStatusException e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1000000.0))
                                .build()));
        assertTrue(e.getMessage().contains("Insufficient balance to transfer coin"));
        verify(walletAccountsDao, never()).save(any(WalletAccounts.class));

        // Valid Not Call Check Price When is same coin
        service.transfer(1L,
                TransferCoin.builder()
                        .coinBase("BTC")
                        .coinToTransfer("BTC")
                        .wallet(2L)
                        .amount(BigDecimal.valueOf(1.0))
                        .build());
        verify(cryptoCompareService, never()).getPrices(anyString(), anyList());

        e = assertThrows(
                ResponseStatusException.class, () -> service.transfer(1L,
                        TransferCoin.builder()
                                .coinBase("BTC")
                                .coinToTransfer("BCH")
                                .wallet(2L)
                                .amount(BigDecimal.valueOf(1.0))
                                .build()));
        assertTrue(e.getMessage().contains("Invalid Coin, price not found"));
        verify(cryptoCompareService, times(1)).getPrices(anyString(), anyList());
    }

    @Test
    public void whenTransferSucessThenShouldShouldBeReturnNewBalance() throws IOException {
        when(walletDao.findById(anyLong())).thenReturn(Optional.of(Wallet.builder().name("Wallet").mainCoin("BTC").status(1).build()));
        when(coinsDao.findOneBySymbol(anyString())).thenReturn(Optional.of(Coins.builder().symbol("BCH").build()));
        when(walletAccountsDao.findOneByWalletAndCoin(any(Wallet.class), anyString()))
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").id(1L).build()))
                .thenReturn(Optional.empty())
                .thenReturn(Optional.of(WalletAccounts.builder().balance(BigDecimal.valueOf(100.0)).coin("BTC").id(1L).build()))
                .thenReturn(Optional.empty());
        when(cryptoCompareService.getPrices(anyString(), anyList())).thenReturn(singletonMap("BTC", 0.03116));
        when(walletAccountsDao.save(any(WalletAccounts.class)))
                .then((invocationOnMock) -> {
                    WalletAccounts accounts = (WalletAccounts) invocationOnMock.getArguments()[0];
                    assertEquals(accounts.getBalance(), BigDecimal.valueOf(2.0));
                    return accounts;
                })
                .then((invocationOnMock) -> invocationOnMock.getArguments()[0])
                .then((invocationOnMock) -> {
                    WalletAccounts accounts = (WalletAccounts) invocationOnMock.getArguments()[0];
                    assertEquals(accounts.getBalance(), BigDecimal.valueOf(2.0));
                    return accounts;
                })
                .then((invocationOnMock) -> invocationOnMock.getArguments()[0]);

        WalletAccounts accounts = service.transfer(1L,
                TransferCoin.builder()
                        .coinBase("BTC")
                        .coinToTransfer("BCH")
                        .wallet(2L)
                        .amount(BigDecimal.valueOf(2.0))
                        .build());

        assertEquals(accounts.getBalance(), BigDecimal.valueOf(100.0).subtract(BigDecimal.valueOf(2.0).multiply(BigDecimal.valueOf(0.03116))));
        assertNotNull(accounts.getCurrentPrice());

        // Same Coin
        accounts = service.transfer(1L,
                TransferCoin.builder()
                        .coinBase("BTC")
                        .coinToTransfer("BTC")
                        .wallet(2L)
                        .amount(BigDecimal.valueOf(2.0))
                        .build());

        assertEquals(accounts.getBalance(), BigDecimal.valueOf(100.0).subtract(BigDecimal.valueOf(2.0)));
        assertNull(accounts.getCurrentPrice());
    }
}
