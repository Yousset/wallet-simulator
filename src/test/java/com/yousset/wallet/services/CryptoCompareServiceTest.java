package com.yousset.wallet.services;

import com.yousset.wallet.api.ExceptionApi;
import com.yousset.wallet.api.models.ResponseCoinList;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@SuppressWarnings("ALL")
@SpringBootTest(
        properties = {"myconfig.initialize.coins=false"}
)
public class CryptoCompareServiceTest {

    private static final String RESULT_OK_BODY =
            "{\"Response\":\"Success\",\"Message\":\"Coin list succesfully returned!\",\"Data\":{\"BTC\":{\"Id\":\"1182\",\"Url\":\"/coins/btc/overview\",\"ImageUrl\":\"/media/19633/btc.png\",\"Name\":\"BTC\",\"Symbol\":\"BTC\",\"CoinName\":\"Bitcoin\",\"FullName\":\"Bitcoin (BTC)\",\"Algorithm\":\"SHA-256\"}},\"BaseImageUrl\":\"https://www.cryptocompare.com\",\"BaseLinkUrl\":\"https://www.cryptocompare.com\",\"HasWarning\":false}";

    private static final String RESULT_OK_BODY_PRICES =
            "{\"USD\":0.007725,\"EUR\":0.007149,\"ARS\":0.8937}";

    private static final String RESULT_OK_BODY_PRICES_ERROR =
            "{\"Response\":\"Error\",\"Message\":\"Coin list succesfully returned!\"}";

    @Autowired
    CryptoCompareService service;
    @MockBean
    HttpClient httpClient;

    @Test
    public void whenGetAllCurrenciesIsCode200ThenShouldReturnList() throws IOException, InterruptedException {
        loadRestConfig(OK, RESULT_OK_BODY, 0);

        ResponseCoinList response = service.getAllCurrencies();
        assertNotNull(response);
        assertEquals(response.getData().get("BTC").getSymbol(), "BTC");
        verify(httpClient, times(1)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetAllCurrenciesIsCode200ButBodyIsInvalidThenShouldThrowException() throws IOException, InterruptedException {
        loadRestConfig(OK, "{fake", 0);
        assertThrows(
                ExceptionApi.class, () -> service.getAllCurrencies());
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetAllCurrenciesIsCodeDistinct200ThenShouldThrowException() throws IOException, InterruptedException {
        loadRestConfig(NOT_FOUND, RESULT_OK_BODY, 0);
        assertThrows(
                ExceptionApi.class, () -> service.getAllCurrencies());
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetAllCurrenciesRetryThenShouldReturnList() throws IOException, InterruptedException {
        loadRestConfig(OK, RESULT_OK_BODY, 2);

        ResponseCoinList response = service.getAllCurrencies();
        assertNotNull(response);
        assertEquals(response.getData().get("BTC").getSymbol(), "BTC");
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetAllCurrenciesRetryFailThenShouldThrowException() throws IOException, InterruptedException {
        loadRestConfig(OK, RESULT_OK_BODY, 5);
        assertThrows(
                ExceptionApi.class, () -> service.getAllCurrencies());
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetPricesIsCode200ThenShouldReturnList() throws IOException, InterruptedException {
        loadRestConfig(OK, RESULT_OK_BODY_PRICES, 0);

        Map<String, Double> response = service.getPrices("BTC", Collections.singletonList("USD"));
        assertNotNull(response);
        assertEquals(response.get("USD"), 0.007725);
        verify(httpClient, times(1)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetPricesIsCode200ButResponseErrorThenShouldReturnNull() throws IOException, InterruptedException {
        loadRestConfig(OK, RESULT_OK_BODY_PRICES_ERROR, 0);

        Map<String, Double> response = service.getPrices("BTC", Collections.singletonList("USD"));
        assertNull(response);
        verify(httpClient, times(1)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetPricesIsCode200ButBodyIsInvalidThenShouldThrowException() throws IOException, InterruptedException {
        loadRestConfig(OK, "{fake", 0);
        assertThrows(
                ExceptionApi.class, () -> service.getPrices("BTC", Collections.singletonList("USD")));
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetPricesIsCodeDistinct200ThenShouldThrowException() throws IOException, InterruptedException {
        loadRestConfig(NOT_FOUND, RESULT_OK_BODY_PRICES, 0);
        assertThrows(
                ExceptionApi.class, () -> service.getPrices("BTC", Collections.singletonList("USD")));
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetPricesRetryThenShouldReturnList() throws IOException, InterruptedException {
        loadRestConfig(OK, RESULT_OK_BODY_PRICES, 2);

        Map<String, Double> response = service.getPrices("BTC", Collections.singletonList("USD"));
        assertNotNull(response);
        assertEquals(response.get("USD"), 0.007725);
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    @Test
    public void whenGetPricesRetryFailThenShouldThrowException() throws IOException, InterruptedException {
        loadRestConfig(OK, RESULT_OK_BODY_PRICES, 5);
        assertThrows(
                ExceptionApi.class, () -> service.getPrices("BTC", Collections.singletonList("USD")));
        verify(httpClient, times(3)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }

    private void loadRestConfig(HttpStatus code, String responseBody, int fails) throws IOException, InterruptedException {
        HttpResponse<String> response = mock(HttpResponse.class);
        when(response.statusCode()).thenReturn(code.value());
        when(response.body()).thenReturn(StringUtils.isEmpty(responseBody) ? RESULT_OK_BODY : responseBody);

        OngoingStubbing<HttpResponse<String>> opt = when(httpClient.send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class)));
        for (int i = 0; i < fails; i++) {
            opt = opt.thenThrow(new RuntimeException("Fake"));
        }
        opt.thenReturn(response);
    }

}
