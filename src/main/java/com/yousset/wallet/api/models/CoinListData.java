package com.yousset.wallet.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CoinListData implements Serializable {

    @JsonProperty("Id")
    private String id;
    @JsonProperty("Url")
    private String url;
    @JsonProperty("ImageUrl")
    private String imageUrl;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Symbol")
    private String symbol;
    @JsonProperty("CoinName")
    private String coinName;
    @JsonProperty("FullName")
    private String fullName;
    @JsonProperty("Algorithm")
    private String algorithm;
    private Map<String, Double> prices;


}
