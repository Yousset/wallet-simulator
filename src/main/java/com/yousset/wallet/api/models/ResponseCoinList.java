package com.yousset.wallet.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yousset.wallet.models.Coins;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseCoinList implements Serializable {

    @JsonProperty("Response")
    private String response;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("Data")
    private Map<String, Coins> data;
    @JsonProperty("BaseImageUrl")
    private String baseImageUrl;
    @JsonProperty("BaseLinkUrl")
    private String baseLinkUrl;
    @JsonProperty("HasWarning")
    private Boolean hasWarning;

}
