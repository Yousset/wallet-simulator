package com.yousset.wallet.api;

public class ExceptionApi extends RuntimeException {
    public ExceptionApi() {
    }

    public ExceptionApi(String message) {
        super(message);
    }

    public ExceptionApi(String message, Throwable cause) {
        super(message, cause);
    }
}
