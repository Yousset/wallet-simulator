package com.yousset.wallet.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Validated
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BuyCoin {

    private String coinBase;

    @NotNull
    private String coinToBuy;

    @NotNull
    @DecimalMin("0.0")
    private BigDecimal amount;
}
