package com.yousset.wallet.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Validated
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateWallet {

    @NotNull
    private String name;
    @NotNull
    private String mainCoin;
    @NotNull
    @DecimalMin("0.0")
    private Double initialBalance;

}
