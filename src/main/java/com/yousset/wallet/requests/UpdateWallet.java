package com.yousset.wallet.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateWallet {

    @NotNull
    private String name;
}
