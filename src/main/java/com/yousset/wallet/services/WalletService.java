package com.yousset.wallet.services;

import com.yousset.wallet.api.models.ResponseCoinList;
import com.yousset.wallet.daos.CoinsDao;
import com.yousset.wallet.daos.WalletAccountsDao;
import com.yousset.wallet.daos.WalletDao;
import com.yousset.wallet.models.Coins;
import com.yousset.wallet.models.Wallet;
import com.yousset.wallet.models.WalletAccounts;
import com.yousset.wallet.requests.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpStatus.*;

@SuppressWarnings("ALL")
@Service
@Slf4j
public class WalletService {

    static final String KEY_PRICE = "prices";

    @Autowired
    CryptoCompareService cryptoCompareService;
    @Autowired
    CoinsDao coinsDao;
    @Autowired
    WalletDao walletDao;
    @Autowired
    WalletAccountsDao walletAccountsDao;
    @Autowired
    CacheManager cacheManager;

    @Autowired
    @Qualifier("pricesExecutor")
    ExecutorService executorService;

    @Value("${myconfig.initialize.coins:true}")
    private boolean initializeCoins;
    @Value("${myconfig.load.prices:true}")
    private boolean loadPrices;
    private boolean loadingPrice = false;

    @PostConstruct
    void postConstruct() {
        CompletableFuture.supplyAsync(() -> {
            if (initializeCoins) {
                loadCurrencyPrice(coinsDao.count() <= 0);
            } else {
                cacheManager.getCache("currencyPrices").put(KEY_PRICE, coinsDao.findAll());
            }
            return null;
        });
    }

    public Iterable<Coins> getAllCurrencyPrice() {
        Cache.ValueWrapper value = cacheManager.getCache("currencyPrices").get(KEY_PRICE);
        if (value == null) {
            if (!loadingPrice) {
                CompletableFuture.supplyAsync(() -> {
                    loadCurrencyPrice(false);
                    return null;
                });
            }
            return coinsDao.findAll();
        }
        return (Iterable<Coins>) value.get();
    }

    @Cacheable(cacheNames = "singleCurrencyPrice", key = "#currency")
    public Map<String, Double> getSingleCurrencyPrice(String currency, List<String> checkPrices) {
        return cryptoCompareService.getPrices(currency, checkPrices);
    }

    @Transactional
    public Coins findCoin(String symbol) {
        return coinsDao
                .findOneBySymbol(symbol)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Invalid Coin Symbol Code"));
    }

    @Transactional
    public Wallet findWallet(Long id) {
        return walletDao
                .findById(id)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Wallet not found"));
    }

    @Transactional
    public Wallet createWallet(CreateWallet createWallet) {
        Coins coins = findCoin(createWallet.getMainCoin());

        Wallet wallet = new Wallet();
        wallet.setMainCoin(coins.getSymbol());
        wallet.setStatus(1);
        wallet.setName(createWallet.getName());

        wallet = walletDao.save(wallet);

        WalletAccounts accounts = new WalletAccounts();
        accounts.setWallet(wallet);
        accounts.setCoin(coins.getSymbol());
        accounts.setBalance(BigDecimal.valueOf(createWallet.getInitialBalance()));

        walletAccountsDao.save(accounts);

        return wallet;
    }

    @Transactional
    public Wallet updateWallet(Long id, UpdateWallet updateWallet) {
        Wallet wallet = findWallet(id);
        wallet.setName(updateWallet.getName());
        return walletDao.save(wallet);
    }

    @Transactional
    public Wallet deleteWallet(Long id) {
        Wallet wallet = findWallet(id);
        wallet.setStatus(0);
        return walletDao.save(wallet);
    }

    @Transactional
    public Iterable<Wallet> getWallets(boolean searchActives, Pageable pageable) {
        if (searchActives) {
            return walletDao.findAllByStatusEquals(1, pageable);
        }
        return walletDao.findAll(pageable);
    }

    @Transactional
    public WalletAccounts debitCreditWallet(Long id, DebitCreditWallet debitCreditWallet, boolean isCredit) {
        Wallet wallet = findWallet(id);
        if (wallet.getStatus() != 1) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "your wallet is deleted");
        }

        String coin = StringUtils.isEmpty(debitCreditWallet.getCoin()) ? wallet.getMainCoin() : debitCreditWallet.getCoin();

        // Valid coin
        findCoin(coin);

        WalletAccounts account = walletAccountsDao
                .findOneByWalletAndCoin(wallet, coin)
                .orElse(WalletAccounts.builder()
                        .balance(BigDecimal.ZERO)
                        .coin(coin)
                        .wallet(wallet).build());

        if (isCredit) {
            account.setBalance(account.getBalance().add(BigDecimal.valueOf(debitCreditWallet.getBalance())));
        } else {
            if (account.getBalance()
                    .subtract(BigDecimal.valueOf(debitCreditWallet.getBalance()))
                    .doubleValue() < 0.0) {
                throw new ResponseStatusException(BAD_REQUEST, "your account cannot be negative");
            }
            account.setBalance(account.getBalance().subtract(BigDecimal.valueOf(debitCreditWallet.getBalance())));
        }
        account = walletAccountsDao.save(account);
        return account;
    }

    @Transactional
    public WalletAccounts buyCoin(Long id, BuyCoin buyCoin) {
        Wallet wallet = findWallet(id);
        if (wallet.getStatus() != 1) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "your wallet is deleted");
        }
        String coin = StringUtils.isEmpty(buyCoin.getCoinBase()) ? wallet.getMainCoin() : buyCoin.getCoinBase();

        // Valid coin
        findCoin(coin);

        //Account Base
        WalletAccounts account = walletAccountsDao
                .findOneByWalletAndCoin(wallet, coin)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Wallet Account: " + coin + " not found"));

        // Valid coin to buy
        findCoin(buyCoin.getCoinToBuy());

        //Account to Buy
        WalletAccounts accountBuy = walletAccountsDao
                .findOneByWalletAndCoin(wallet, buyCoin.getCoinToBuy())
                .orElse(WalletAccounts.builder()
                        .balance(BigDecimal.ZERO)
                        .coin(buyCoin.getCoinToBuy())
                        .wallet(wallet).build());


        Map<String, Double> prices = cryptoCompareService.getPrices(accountBuy.getCoin(), singletonList(account.getCoin()));
        if (prices == null || prices.get(account.getCoin()) == null) {
            throw new ResponseStatusException(NOT_FOUND, "Invalid Coin, price not found");
        }

        BigDecimal price = buyCoin.getAmount()
                .multiply(BigDecimal.valueOf(prices.get(account.getCoin())));

        if (price.compareTo(account.getBalance()) >= 1) {
            throw new ResponseStatusException(BAD_REQUEST, "Insufficient balance to buy coin");
        }

        account.setBalance(account.getBalance().subtract(price));
        accountBuy.setBalance(accountBuy.getBalance().add(buyCoin.getAmount()));

        account = walletAccountsDao.save(account);

        accountBuy = walletAccountsDao.save(accountBuy);
        accountBuy.setCurrentPrice(prices);
        return accountBuy;
    }

    @Transactional
    public WalletAccounts transfer(Long id, TransferCoin transferCoin) {
        Wallet wallet = findWallet(id);
        if (wallet.getStatus() != 1) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "your wallet is deleted");
        }

        Wallet walletTransfer = findWallet(transferCoin.getWallet());
        if (walletTransfer.getStatus() != 1) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Wallet to transfer is deleted");
        }

        String coin = StringUtils.isEmpty(transferCoin.getCoinBase()) ? wallet.getMainCoin() : transferCoin.getCoinBase();

        // Valid coin
        findCoin(coin);

        //Account Base
        WalletAccounts account = walletAccountsDao
                .findOneByWalletAndCoin(wallet, coin)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Wallet Account: " + coin + " not found"));

        //Valid Amount
        if (transferCoin.getAmount().compareTo(account.getBalance()) >= 1) {
            throw new ResponseStatusException(BAD_REQUEST, "Insufficient balance to transfer coin");
        }

        //Account to Buy
        String coinTransfer = StringUtils.isEmpty(transferCoin.getCoinToTransfer()) ? walletTransfer.getMainCoin() : transferCoin.getCoinToTransfer();

        // Valid coin transfer
        findCoin(coinTransfer);

        WalletAccounts accountTransfer = walletAccountsDao
                .findOneByWalletAndCoin(walletTransfer, coinTransfer)
                .orElse(WalletAccounts.builder()
                        .balance(BigDecimal.ZERO)
                        .coin(coinTransfer)
                        .wallet(wallet).build());

        BigDecimal price;
        Map<String, Double> prices = null;
        if (account.getCoin().equalsIgnoreCase(accountTransfer.getCoin())) {
            price = transferCoin.getAmount();
        } else {
            prices = cryptoCompareService.getPrices(accountTransfer.getCoin(), singletonList(account.getCoin()));
            if (prices == null || prices.get(account.getCoin()) == null) {
                throw new ResponseStatusException(NOT_FOUND, "Invalid Coin, price not found");
            }
            price = transferCoin.getAmount()
                    .multiply(BigDecimal.valueOf(prices.get(account.getCoin())));
        }

        if (price.compareTo(account.getBalance()) >= 1) {
            throw new ResponseStatusException(BAD_REQUEST, "Insufficient balance to transfer coin");
        }

        account.setBalance(account.getBalance().subtract(price));
        accountTransfer.setBalance(accountTransfer.getBalance().add(transferCoin.getAmount()));

        accountTransfer = walletAccountsDao.save(accountTransfer);
        account = walletAccountsDao.save(account);
        account.setCurrentPrice(prices);
        return account;
    }

    void loadCurrencyPrice(boolean isNew) {
        loadingPrice = true;
        log.info("Loading Prices");
        try {
            ResponseCoinList response = cryptoCompareService.getAllCurrencies();

            Iterable<Coins> values = response.getData().values();
            if (isNew) {
                values = coinsDao.saveAll(values);
            }

            if (loadPrices) {
                log.info("Saving Prices");
                List<CompletableFuture<Void>> list = new ArrayList<>();
                values.forEach(value -> {
                    list.add(
                            CompletableFuture.supplyAsync(() -> {
                                value.setPrices(getSingleCurrencyPrice(value.getSymbol(), Arrays.asList("USD", "EUR", "ARS", "BTC")));
                                coinsDao.save(value);
                                return null;
                            }, executorService));
                });
                CompletableFuture.allOf(list.toArray(new CompletableFuture[0])).join();
                cacheManager.getCache("currencyPrices").put(KEY_PRICE, coinsDao.findAll());
            }
        } finally {
            loadingPrice = false;
        }
    }
}
