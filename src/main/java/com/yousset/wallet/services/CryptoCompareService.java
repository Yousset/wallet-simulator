package com.yousset.wallet.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.yousset.wallet.api.ExceptionApi;
import com.yousset.wallet.api.models.ResponseCoinList;
import com.yousset.wallet.utils.UncheckedObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriUtils;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Service
@Slf4j
public class CryptoCompareService {

    @Autowired
    private HttpClient httpClient;

    @Autowired
    private UncheckedObjectMapper objectMapper;

    @Value("${currency-api.base-url}")
    private String baseUrl;
    @Value("${currency-api.apikey}")
    private String apiKey;

    @Retryable(value = {RuntimeException.class}, maxAttempts = 3, backoff = @Backoff(delay = 1000, maxDelay = 3000))
    public ResponseCoinList getAllCurrencies() {
        try {
            HttpRequest request = getBuilder(baseUrl + "/data/all/coinlist")
                    .timeout(Duration.ofMinutes(1))
                    .GET().build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (isValidStatusCode(response)) {
                return objectMapper.readValueUnFailed(response.body(), ResponseCoinList.class);
            }
        } catch (Exception e) {
            log.error("Error getAllCurrencies", e);
            throw new ExceptionApi("Error", e);
        }

        return null;
    }

    @Retryable(value = {RuntimeException.class}, maxAttempts = 3, backoff = @Backoff(delay = 1000, maxDelay = 3000))
    public Map<String, Double> getPrices(String currency, List<String> checkPrices) {
        try {
            HttpRequest request = getBuilder(
                    format(baseUrl + "/data/price?fsym=%s&tsyms=%s",
                            UriUtils.encode(currency.trim(), "UTF-8"),
                            String.join(",", checkPrices)
                    ))
                    .GET().build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (isValidStatusCode(response)) {
                    if (response.body().contains("\"Response\":\"Error\"")) {
                    log.error("Fail Call code: {} body {}", response.statusCode(), response.body());
                    return null;
                }

                return objectMapper.readValueUnFailed(response.body(), new TypeReference<Map<String, Double>>() {
                });
            }
        } catch (Exception e) {
            log.error("Error getPrices", e);
            throw new ExceptionApi("Error", e);
        }

        return null;
    }

    private boolean isValidStatusCode(HttpResponse<?> response) {
        if (response == null) return false;

        if (response.statusCode() >= 200 && response.statusCode() < 300) {
            return true;
        }

        log.error("Fail Call code: {}", response.statusCode());
        throw new ExceptionApi("Retry Invalid Status");
    }

    private HttpRequest.Builder getBuilder(String url) {
        return HttpRequest.newBuilder()
                .uri(URI.create(url))
                .timeout(Duration.ofSeconds(5))
                .header("Content-Type", "application/json")
                .header("Authorization", "Apikey " + apiKey);
    }
}
