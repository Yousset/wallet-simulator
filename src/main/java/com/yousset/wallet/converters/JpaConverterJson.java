package com.yousset.wallet.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.LinkedHashMap;

/**
 * @author yousset.chacon
 */
@Converter
public class JpaConverterJson implements AttributeConverter<Object, String> {

    private final static ObjectMapper MAPPER = new ObjectMapper()
            .configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    ;

    @Override
    public String convertToDatabaseColumn(Object meta) {
        if (meta == null) {
            return null;
        }

        try {
            return MAPPER.writeValueAsString(meta);
        } catch (Throwable ex) {
            return null;
        }
    }

    @Override
    public Object convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }

        try {
            LinkedHashMap<String, Object> map = MAPPER.readValue(dbData, new TypeReference<LinkedHashMap<String, Object>>() {
            });
            return map;
        } catch (Throwable ex) {
            return null;
        }
    }
}
