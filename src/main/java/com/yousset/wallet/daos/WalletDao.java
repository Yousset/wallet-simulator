package com.yousset.wallet.daos;

import com.yousset.wallet.models.Wallet;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WalletDao extends PagingAndSortingRepository<Wallet, Long> {

    List<Wallet> findAllByStatusEquals(Integer status, Pageable pageable);

}
