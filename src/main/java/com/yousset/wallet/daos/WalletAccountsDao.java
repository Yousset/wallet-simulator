package com.yousset.wallet.daos;

import com.yousset.wallet.models.Wallet;
import com.yousset.wallet.models.WalletAccounts;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WalletAccountsDao extends PagingAndSortingRepository<WalletAccounts, Long> {

    Optional<WalletAccounts> findOneByWalletAndCoin(Wallet wallet, String coin);
}
