package com.yousset.wallet.daos;

import com.yousset.wallet.models.Coins;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CoinsDao extends PagingAndSortingRepository<Coins, String> {

    Optional<Coins> findOneBySymbol(String symbol);
}
