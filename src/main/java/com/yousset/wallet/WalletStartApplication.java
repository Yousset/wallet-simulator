package com.yousset.wallet;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ybelandria
 */
@SpringBootApplication
@Slf4j
public class WalletStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(WalletStartApplication.class, args);
    }
}
