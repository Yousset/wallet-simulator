package com.yousset.wallet.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.yousset.wallet.models.json_views.EntityView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true, value = {"hibernateLazyInitializer", "handler"})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Wallet {

    @Id
    @GeneratedValue
    @JsonView(EntityView.Basic.class)
    private Long id;
    @JsonView(EntityView.Basic.class)
    private String name;
    @JsonView(EntityView.Basic.class)
    private String mainCoin;
    @JsonView(EntityView.Basic.class)
    private Integer status;
    @OneToMany(mappedBy = "wallet", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonView({EntityView.Complete.class})
    private List<WalletAccounts> accounts;
}
