package com.yousset.wallet.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yousset.wallet.converters.JpaConverterJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true, value = {"hibernateLazyInitializer", "handler"})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Coins {

    @Id
    @JsonProperty("Id")
    private String id;

    @JsonProperty("Url")
    private String url;

    @JsonProperty("ImageUrl")
    private String imageUrl;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Symbol")
    private String symbol;

    @JsonProperty("CoinName")
    private String coinName;

    @JsonProperty("FullName")
    private String fullName;

    @JsonProperty("Algorithm")
    private String algorithm;

    @Column(length = 1000, name = "prices")
    @Convert(converter = JpaConverterJson.class)
    private Map<String, Double> prices;
}
