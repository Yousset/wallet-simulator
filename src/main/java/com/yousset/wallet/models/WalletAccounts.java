package com.yousset.wallet.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.yousset.wallet.models.json_views.AccountsEntityView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true, value = {"hibernateLazyInitializer", "handler"})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class WalletAccounts {

    @Id
    @GeneratedValue
    private Long id;
    private String coin;
    private BigDecimal balance;
    @JoinColumn(updatable = false)
    @ManyToOne(optional = false)
    @JsonView({AccountsEntityView.Complete.class})
    private Wallet wallet;
    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Double> currentPrice;
}
