package com.yousset.wallet.config;

import com.google.common.cache.CacheBuilder;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CachingConfig {

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("currencyPrices", "singleCurrencyPrice") {

            @Override
            protected Cache createConcurrentMapCache(final String name) {
                int time = 5;
                if ("currencyPrices".equalsIgnoreCase(name)) {
                    time = 30;
                }

                return new ConcurrentMapCache(name,
                        CacheBuilder.newBuilder()
                                .expireAfterWrite(time, TimeUnit.MINUTES)
                                .build().asMap(),
                        false);
            }
        };
    }
}
