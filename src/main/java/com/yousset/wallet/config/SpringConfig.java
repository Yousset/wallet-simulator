package com.yousset.wallet.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.yousset.wallet.utils.UncheckedObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

import java.net.http.HttpClient;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static java.net.http.HttpClient.Redirect.NORMAL;
import static java.net.http.HttpClient.Version.HTTP_2;

@Configuration
@EnableRetry
@Slf4j
public class SpringConfig {

    @Bean
    public HttpClient getHttpClient() {
        return HttpClient.newBuilder()
                .version(HTTP_2)
                .followRedirects(NORMAL)
                .build();
    }

    @Bean
    public UncheckedObjectMapper getMapper() {
        return (UncheckedObjectMapper) new UncheckedObjectMapper()
                .configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Bean(name = "pricesExecutor")
    public ExecutorService buildExecutorPrices() {
        return new ThreadPoolExecutor(
                10,
                10,
                0,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(11000),
                new ThreadFactoryBuilder()
                        .setUncaughtExceptionHandler((t, e) -> log.error("Error Thread " + t.getName(), e))
                        .setNameFormat("prices-thread-%d")
                        .build());
    }
}

