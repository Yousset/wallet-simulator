package com.yousset.wallet.config;

import com.yousset.wallet.config.filter.APIKeyAuthFilter;
import com.yousset.wallet.controllers.HealthCheckController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletResponse;

/**
 * Global security configuration
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${xclient.header:wallet}")
    private String xClientHeader;

    @Value("${xclient.header_value:yousset}")
    private String xClientHeaderValue;

    /**
     * URL paths to consider when working on interceptors or security configurations
     */
    public static final String[] ALLOWED_PATH_PATTERNS = {
            "/resources/**",
            "/static/**",
            "/js/**",
            "/css/**",
            "/scss/**",
            "/img/**",
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/health-check/**",
            "/h2/**",
            "/"
    };

    @Autowired
    public WebSecurityConfig() {
    }

    @Override
    public void configure(WebSecurity web) {
        /* Ignoring static resources typically used by swagger */
        web.ignoring().antMatchers(ALLOWED_PATH_PATTERNS);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        APIKeyAuthFilter filter = new APIKeyAuthFilter(xClientHeader);
        filter.setAuthenticationManager(authentication -> {
            String principal = (String) authentication.getPrincipal();
            if (!xClientHeaderValue.equals(principal)) {
                throw new BadCredentialsException("The API key was not found or not the expected value.");
            }
            authentication.setAuthenticated(true);
            return authentication;
        });


        AuthenticationEntryPoint unauthorizedEntryPoint =
                (request, response, e) ->
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");

        http
                // disabling csrf
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedEntryPoint)
                .and()
                // no sessions
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HealthCheckController.ENDPOINT)
                .permitAll()
                // Uncomment This for add Security by header content
                // .and().addFilter(filter).authorizeRequests().anyRequest().authenticated()
                .and().httpBasic();
    }
}
