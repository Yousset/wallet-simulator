package com.yousset.wallet.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

    public static final String ENDPOINT = "/health-check";

    @GetMapping(ENDPOINT)
    public String healthcheck() {
        return "OK";
    }
}
