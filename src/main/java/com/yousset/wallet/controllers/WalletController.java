package com.yousset.wallet.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.yousset.wallet.models.Coins;
import com.yousset.wallet.models.Wallet;
import com.yousset.wallet.models.WalletAccounts;
import com.yousset.wallet.models.json_views.AccountsEntityView;
import com.yousset.wallet.models.json_views.EntityView;
import com.yousset.wallet.requests.*;
import com.yousset.wallet.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequestMapping(value = "/api")
@RestController
public class WalletController {

    @Autowired
    WalletService walletService;

    @GetMapping(value = "/coins-prices",
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Coins>> coinsPrices() {
        return new ResponseEntity<>(
                walletService.getAllCurrencyPrice(),
                HttpStatus.OK
        );
    }

    @PostMapping(value = "/wallet",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @JsonView(EntityView.Basic.class)
    public ResponseEntity<Wallet> create(@Valid @RequestBody CreateWallet body) {
        return new ResponseEntity<>(
                walletService.createWallet(body),
                HttpStatus.OK
        );
    }

    @GetMapping(value = "/wallet",
            produces = APPLICATION_JSON_VALUE)
    @JsonView(EntityView.Basic.class)
    public ResponseEntity<Iterable<Wallet>> walletsActives(Pageable pageable) {
        return new ResponseEntity<>(
                walletService.getWallets(true, pageable),
                HttpStatus.OK
        );
    }

    @GetMapping(value = "/wallet-all",
            produces = APPLICATION_JSON_VALUE)
    @JsonView(EntityView.Basic.class)
    public ResponseEntity<Iterable<Wallet>> allWallets(Pageable pageable) {
        return new ResponseEntity<>(
                walletService.getWallets(false, pageable),
                HttpStatus.OK
        );
    }

    @GetMapping(value = "/wallet/{id}",
            produces = APPLICATION_JSON_VALUE)
    @JsonView(EntityView.Complete.class)
    public ResponseEntity<Wallet> oneWallet(@PathVariable Long id) {
        return new ResponseEntity<>(
                walletService.findWallet(id),
                HttpStatus.OK
        );
    }

    @PutMapping(value = "/wallet/{id}",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @JsonView(EntityView.Basic.class)
    public ResponseEntity<Wallet> updateWallet(@PathVariable Long id, @Valid @RequestBody UpdateWallet body) {
        return new ResponseEntity<>(
                walletService.updateWallet(id, body),
                HttpStatus.OK
        );
    }

    @DeleteMapping(value = "/wallet/{id}",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @JsonView(EntityView.Basic.class)
    public ResponseEntity<Wallet> deleteWallet(@PathVariable Long id) {
        return new ResponseEntity<>(
                walletService.deleteWallet(id),
                HttpStatus.OK
        );
    }

    @PostMapping(value = "/wallet/{id}/debit",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @JsonView(AccountsEntityView.Complete.class)
    public ResponseEntity<WalletAccounts> debit(@PathVariable Long id, @Valid @RequestBody DebitCreditWallet body) {
        return new ResponseEntity<>(
                walletService.debitCreditWallet(id, body, false),
                HttpStatus.OK
        );
    }

    @PostMapping(value = "/wallet/{id}/credit",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @JsonView(AccountsEntityView.Complete.class)
    public ResponseEntity<WalletAccounts> credit(@PathVariable Long id, @Valid @RequestBody DebitCreditWallet body) {
        return new ResponseEntity<>(
                walletService.debitCreditWallet(id, body, true),
                HttpStatus.OK
        );
    }

    @PostMapping(value = "/wallet/{id}/buy",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @JsonView(AccountsEntityView.Complete.class)
    public ResponseEntity<WalletAccounts> buyCoin(@PathVariable Long id, @Valid @RequestBody BuyCoin body) {
        return new ResponseEntity<>(
                walletService.buyCoin(id, body),
                HttpStatus.OK
        );
    }

    @PostMapping(value = "/wallet/{id}/transfer",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @JsonView(AccountsEntityView.Complete.class)
    public ResponseEntity<WalletAccounts> transfer(@PathVariable Long id, @Valid @RequestBody TransferCoin body) {
        return new ResponseEntity<>(
                walletService.transfer(id, body),
                HttpStatus.OK
        );
    }
}
